const { Actions, Builder, Button, By, Key, until, WebElement, promise } = require('selenium-webdriver');
const display = require('./display');

const minefieldTerms = {
    mines_ones: 'mines_ones',
    mines_tens: 'mines_tens',
    mines_hundreds: 'mines_hundreds',
    bombflagged: 9
};

let driver;

let mvars = {
    rows: 16,
    cur_row: 8,
    cols: 30,
    cur_col:15,
    mines_max: 99,
    mines_left: 99,
    max_open: undefined,
    open: 0,
    matrix: undefined,
    game_running: true
};

console.log('program started');

const generateMinefieldMatrix = () => {
    mvars.matrix = [];
    for (let i = 0;i < mvars.rows; i++) {
        mvars.matrix.push([]);
        for (let j = 0;j < mvars.cols; j++) {
            mvars.matrix[i][j] = undefined;
        }
    }
    // console.log({matrix: mvars.matrix});
}

const getRevealedMinecount = async () => {
    const we_mines = {
        hundreds: await driver.findElement(By.id(minefieldTerms.mines_hundreds)).getAttribute('class'),
        tens: await driver.findElement(By.id(minefieldTerms.mines_tens)).getAttribute('class'),
        ones: await driver.findElement(By.id(minefieldTerms.mines_ones)).getAttribute('class')
    };
    we_mines.hundreds = parseInt(we_mines.hundreds.substring(4));
    we_mines.tens = parseInt(we_mines.tens.substring(4));
    we_mines.ones = parseInt(we_mines.ones.substring(4));
    const mine_amount = (we_mines.hundreds * 100) + (we_mines.tens * 10) + we_mines.ones;
    mvars.mines_left = mine_amount;
    // console.log({we_mines, mine_amount});
};

const getCurrent = async (row, col) => {
    row = (row + 1).toString();
    col = (col + 1).toString();
    const unparsedMineBlock = await driver.findElement(By.id(`${row}_${col}`)).getAttribute('class');
    const parsedMineblock = unparsedMineBlock.substring(7);
    const block_options = [
        'bombdeath',
        'blank',
        'open0',
        'open1',
        'open2',
        'open3',
        'open4',
        'open5',
        'open6',
        'open7',
        'open8',
        'bombflagged',
        'bombrevealed',
    ];
    for (let i = 0; i < block_options.length; i++) {
        if (parsedMineblock.includes(block_options[i])) {
            if ((i-2) === -2) {
                mvars.game_running = false;
            }
            return i - 2; // -2 = dead, -1 = blank, 0 = open0... 9 = bombflagged, 10 = victory or loss
        }
    }
    return -3; // error
}
const updateMatrix = async (firsttime) => {
    for (let i = 0;i < mvars.rows; i++) {
        for (let j = 0; j < mvars.cols; j++) {
            if (firsttime) {
                mvars.matrix[i][j] = await getCurrent(i,j).catch((err) => console.log(err));;
            } else {
                if (mvars.matrix[i][j] < 0 || mvars.matrix[i][j] > 9) { // skip unnecessary checks for performance
                    // console.log({msg: 'yes', i, j})
                    mvars.matrix[i][j] = await getCurrent(i,j).catch((err) => console.log(err));;
                } // else {
                    // console.log({msg: 'no', i, j})
                // }
            }
        }
    }
    // display.matrix(mvars.matrix);
}
const clickBlock = async (row, col) => {
    const r = row;
    const c = col;
    row = (row + 1).toString();
    col = (col + 1).toString();
    await driver.findElement(By.id(`${row}_${col}`)).click();
    mvars.open++;
    if (mvars.matrix) {
        mvars.matrix[r][c] = await getCurrent(r,c).catch((err) => console.log(err));;
    }
}

const markMine = async (row, col) => {
    if (mvars.matrix[row][col] === minefieldTerms.bombflagged) return;
    mvars.matrix[row][col] = minefieldTerms.bombflagged;
    row = (row + 1).toString();
    col = (col + 1).toString();
    // const action = new Actions(driver);
    const el = await driver.findElement(By.id(`${row}_${col}`));
    driver.actions({bridge: true}).contextClick(el, Button.RIGHT).perform();
    // action.contextClick(link).perform();
}

/**
 * Makes three-by-three matrix
 * @param {number} crow current row
 * @param {number} ccol current col
 * @return {number[][]} three-by-three matrix
 */
const makeThreeByThree = (crow, ccol) => {
    let up = crow == 0 ? true : false;
    let down = crow == (mvars.rows - 1) ? true : false;
    let left = ccol == 0 ? true : false;
    let right = ccol == (mvars.cols - 1) ? true : false; // warning, may be empty
    if (up || down || left || right) {
        return;
    } else {
        const tbtmatrix = [];
        for (let i=0;i<3;i++) {
            tbtmatrix.push([]);
            for (let j=0;j<3;j++) {
                const r = crow + (i-1);
                const c = ccol + (j-1);
                tbtmatrix[i].push(mvars.matrix[r][c]);
            }
        }
        return tbtmatrix;
    }  
}

/**
 * Makes three-by-four matrix
 * @param {number} crow current row
 * @param {number} ccol current col
 * @param {number[][]} matrix of current whole area
 * @return {number[][]} three-by-four matrix
 */
const makeThreeByFour = (crow, ccol, matrix) => {
    let up = crow == 0 ? true : false;
    let down = crow > (matrix.length - 2) ? true : false;
    let left = ccol == 0 ? true : false;
    let right = ccol > (matrix[0].length - 1) ? true : false;
    if (up || down || left || right) {
        return;
    } else {
        tbfmatrix = [];
        for (let i=0;i<3;i++) {
            tbfmatrix.push([]);
            for (let j=0;j<4;j++) { // threebyfour
                const r = crow + (i-1);
                const c = ccol + (j-1);
                tbfmatrix[i][j] = matrix[r][c];
            }
        }
        return tbfmatrix;
    }
}

/**
 * Makes four-by-three matrix
 * @param {number} crow current row
 * @param {number} ccol current col
 * @param {number[][]} matrix of current whole area
 * @return {number[][]} four-by-three matrix
 */
const makeFourByThree = (crow, ccol, matrix) => {
    let up = crow == 0 ? true : false;
    let down = crow > (matrix.length - 1) ? true : false;
    let left = ccol == 0 ? true : false;
    let right = ccol > (matrix[0].length - 2) ? true : false;
    if (up || down || left || right) {
        return;
    } else {
        fbtmatrix = []; // fourbythree
        for (let i=0;i<4;i++) {
            fbtmatrix.push([]);
            for (let j=0;j<3;j++) {
                const r = crow + (i-1);
                const c = ccol + (j-1);
                fbtmatrix[i][j] = matrix[r][c];
            }
        }
        return fbtmatrix;
    }
}

/**
 * Find mines from three by three matrix
 * @param {number[][]} matrix three by three
 */
const findMinesFromThreeByThree = (matrix) => {
    let offset = [];
    const middle = matrix[1][1];
    if (middle < 1 && middle > 8) return undefined; // no sure mines
    let minecount = 0;
    for (let i=0;i<matrix.length;i++) {
        for (let j=0;j<matrix[i].length;j++) {
            if (matrix[i][j] === -1 || matrix[i][j] === 9) minecount++;
            if (matrix[i][j] === -1) {
                offset.push({
                    row: i - 1,
                    col: j - 1
                });
            }
        }
    }
    if (minecount !== middle) return undefined; // can't confirm mines
    return offset;
}

const transposeMatrix = (matrix) => {
    return matrix[0].map((val, index) => matrix.map(row => row[index]));
}

/**
 * @typedef {Object} MatrixOffset
 * @property {number} row - offset from current row
 * @property {number} col - offset from current col
 * @property {boolean} mine - should be marked as mine
 * @property {boolean} open - should be opened
 */

/**
 * Check mines at three-by-four matrix. can be used to four-by-three matrix solving too.
 * @param {number[][]} matrix three rows and four cols
 * @return {MatrixOffset[]} row and col is offset, should be added to current row / col before next operation.
 */
const findMinesFromThreeByFour = (matrix) => {
    let dir = 'three-by-four';
    if (matrix.length === 4) {
        dir = 'four-by-three'; // for condition checking later at return
        matrix = transposeMatrix(matrix); // rotate matrix
    }
    //#region check uprow
    for (let i=0;i<3;i++) { // any mark ruins the logic
        if (matrix[i].find((markedmine) => markedmine === 9)) return; 
    }
    let uprowBlocks = matrix[0].every((val) => val === -1);
    let leftBlock = matrix[1][0] !== -1 ? true : false;
    let rightBlock = matrix[1][3] !== -1 ? true : false;
    let downrowBlocks = matrix[2].every((val) => val !== -1);

    // Check if ones and twos are together
    if (uprowBlocks && downrowBlocks && leftBlock && rightBlock) { // verify the setup is okay
        // display.matrix(matrix,true);
        if (matrix[1][1] === 1 && matrix[1][2] === 2) { // one two collision up
            const offsets = [];
            if (dir === 'three-by-four') {
                offsets.push({row: -1, col: 2, mine: true, open: false});
                offsets.push({row: -1, col: -1, mine: false, open: true});
            } else if (dir === 'four-by-three') { // one two requires rotate counter-clockwise (switch rows and cols)
                offsets.push({row: 2, col: -1, mine: true, open: false });
                offsets.push({row: -1, col: -1, mine: false, open: true });
            }
            return offsets;
        } else if (matrix[1][1] === 2 && matrix[1][2] === 1) { // two one collision up
            const offsets = [];
            if (dir === 'three-by-four') {
                offsets.push({row: -1, col: -1, mine: true, open: false});
                offsets.push({row: -1, col: 2, mine: false, open: true});
            } else if (dir === 'four-by-three') { // one two requires rotate counter-clockwise (switch rows and cols)
                offsets.push({row: -1, col: -1, mine: true, open: false });
                offsets.push({row: 2, col: -1, mine: false, open: true });
            }
            return offsets;
        }
    }
    //#endregion check uprow
    //#region check downrow
    uprowBlocks = matrix[0].every((val) => val !== -1);
    leftBlock = matrix[1][0] !== -1 ? true : false;
    rightBlock = matrix[1][3] !== -1 ? true : false;
    downrowBlocks = matrix[2].every((val) => val === -1);

    if (uprowBlocks && downrowBlocks && leftBlock && rightBlock) {
        if (matrix[1][1] === 1 && matrix[1][2] === 2) { // one two collision down
            const offsets = [];
            if (dir === 'three-by-four') {
                offsets.push({row: 1, col: 2, mine: true, open: false});
                offsets.push({row: 1, col: -1, mine: false, open: true});
            } else if (dir === 'four-by-three') { // one two requires rotate counter-clockwise (switch rows and cols)
                offsets.push({row: 2, col: 1, mine: true, open: false });
                offsets.push({row: -1, col: 1, mine: false, open: true });
            }
            return offsets;
        } else if (matrix[1][1] === 2 && matrix[1][2] === 1) { // two one collision down
            const offsets = [];
            if (dir === 'three-by-four') {
                offsets.push({row: 1, col: -1, mine: true, open: false});
                offsets.push({row: 1, col: 2, mine: false, open: true});
            } else if (dir === 'four-by-three') { // one two requires rotate counter-clockwise (switch rows and cols)
                offsets.push({row: -1, col: 1, mine: true, open: false });
                offsets.push({row: 2, col: 1, mine: false, open: true });
            }
            return offsets;
        }
    }
    //#endregion check downrow
}

/*const setNext = (col, row) => {
    if (col && row) {
        mvars.cur_col = col;
        mvars.cur_row = row;
        return;
    }
    mvars.cur_col++;
    if (mvars.cur_col == mvars.cols) {
        mvars.cur_col = 0;
        mvars.cur_row++;
        if (mvars.cur_row == mvars.rows) {
            mvars.cur_row = 0;
        }
    }
};*/

/*const isBlock = (row,col,matrix) => {
    matrix[row][col] === -1 ? true : false;
}*/

/*const getNextNonBlock = (crow, ccol, matrix) => {
    const rows = matrix.length;
    const cols = matrix[0].length;
    ccol += 1;
    if (ccol == cols) {
        crow += 1;
        ccol = 0;
        if (crow == rows) {
            crow = 0;
        }
    }
    if (matrix[crow][ccol] === -1) {
        const nextNonBlock = getNextNonBlock(crow,ccol,matrix);
        return nextNonBlock;
    } else {
        const retObj = {
            row: crow,
            col: ccol
        };
        return retObj;
    }
}*/

/**
 * Checks if all nearbys are found
 * @param {Object[]} middle point where it is checked from
 * @param {number} middle.rowindex row location
 * @param {number} middle.colindex col location
 * @param {number[][]} matrix takes in matrix and middle
 */
const isAllMinesMarkedAt = (middle, matrix) => {
    const middleVal = matrix[middle.rowindex][middle.colindex];
    if (middleVal < 1 && middleVal > 8) return false; // is not a spot to check
    let minesmarked = 0;
    for (const row of matrix) {
        for (const col of row) {
            if (col === minefieldTerms.bombflagged) minesmarked++;
        }
    }
    return minesmarked == middleVal ? true : false;
}

const openAfterCheck = async (crow,ccol) => {
    for (let i=0;i<3;i++) {
        for (let j=0;j<3;j++) {
            const target = {
                row: crow+(i-1),
                col: ccol+(j-1)
            }  
            if (mvars.matrix[target.row][target.col] === -1) {
                await clickBlock(target.row, target.col).catch((err) => console.log(err));
            }
        }
    }
}

/**
 * Finds all nearbys, buggy af
 * @returns {Promise<void>[]}
 */
const findAllNearbys = async () => {
    const proms = [];
    for (let i=0;i<mvars.rows;i++) {
        for (let j=0;j<mvars.cols;j++) {
            // skip blocks === -1, empty === 0, minemarks === 9
            if (0 < mvars.matrix[i][j] && mvars.matrix[i][j] < 9) { // do only block borders
                const tbt = makeThreeByThree(i, j);
                const fbt = makeFourByThree(i,j,mvars.matrix);
                const tbf = makeThreeByFour(i,j,mvars.matrix);
                if (tbt) {
                    const tbtOffset = findMinesFromThreeByThree(tbt);
                    if (tbtOffset) {
                        for (const offset of tbtOffset) {
                            const r = offset.row + i;
                            const c = offset.col + j;
                            proms.push(markMine(r,c));
                        }
                    }
                }
                if (fbt) {
                    const fbtOffsets = findMinesFromThreeByFour(fbt);
                    if (fbtOffsets) {
                        // console.log({fbtOffsets});
                        for (const offset of fbtOffsets) {
                            const r = offset.row + i;
                            const c = offset.col + j;
                            if (offset.open == true) {
                                proms.push(clickBlock(r,c));
                            } else if (offset.mine == true) {
                                proms.push(markMine(r,c));
                            }
                        }
                    }
                }
                if (tbf) {
                    const tbfOffsets = findMinesFromThreeByFour(tbf);
                    if (tbfOffsets) {   
                        // console.log({tbfOffsets});
                        for (const offset of tbfOffsets) {
                            const r = offset.row + i;
                            const c = offset.col + j;
                            if (offset.open == true) {
                                proms.push(clickBlock(r,c));
                            } else if (offset.mine == true) {
                                proms.push(markMine(r,c));
                            }
                        }
                    }
                }
            }
        }
    }
    return await Promise.all(proms);
}

const checkAndOpenReadySpots = async () => {
    for (let i=0;i<mvars.rows;i++) {
        for (let j=0;j<mvars.cols;j++) {
            const mid = { rowindex: i, colindex: j};
            const up = i == 0 ? true : false;
            const down = i == (mvars.matrix.length - 1) ? true : false;
            const left = j == 0 ? true : false;
            const right = j == (mvars.matrix[0].length - 1) ? true : false; // warning, may be empty
            if (up||down||left||right) { // TODO: fix
                // console.log('do check updownlefright before opening');
            } else if (mvars.matrix[i][j] < 1 && mvars.matrix[i][j] > 8) { // TODO: fix
                // console.log('not need to check');
            } else {
                const minimatrix = [];
                const minmid = { rowindex: 1, colindex: 1};
                for(let x=0;x<3;x++) {
                    minimatrix.push([]);
                    for(let y=0;y<3;y++) {
                        minimatrix[x].push(mvars.matrix[i+(x-1)][j+(y-1)]);
                    }
                }
                // console.log({minmid, minimatrix});
                const isReady = isAllMinesMarkedAt(minmid, minimatrix);
                if (isReady) {
                    // console.log(`openable spot near ${i}_${j}`);
                    // await clickBlock(i,j);
                    await openAfterCheck(i,j).catch((err) => console.log(err));
                }
                // console.log({readyForOpen: isReady, minimatrix});
            }
        }
    }
}

const runGame = async () => {
    mvars.max_open = (mvars.rows * mvars.cols) - mvars.mines_max;
    await clickBlock(mvars.cur_row, mvars.cur_col).catch((err) => console.log(err));
    generateMinefieldMatrix();
    await updateMatrix(true).catch((err) => console.log(err));
    while (mvars.game_running) {
        // find nearbys
        try {
            await findAllNearbys().catch((err) => console.log(err));
            await updateMatrix().catch((err) => console.log(err));
            display.matrix(mvars.matrix, true);
            await findAllNearbys().catch((err) => console.log(err));
            await updateMatrix().catch((err) => console.log(err));
            display.matrix(mvars.matrix, true);
            await checkAndOpenReadySpots().catch((err) => console.log(err));
            await updateMatrix().catch((err) => console.log(err));
            display.matrix(mvars.matrix, true);
        } catch (err) {
            console.log(err);
            display.matrix(mvars.matrix, true);
            try {
                generateMinefieldMatrix();
                await updateMatrix(true).catch((err) => console.log(err));
                display.matrix(mvars.matrix, true);
            } catch {
                console.log('comeon');
            }
        }
    }
    quitGame();
};

const quitGame = () => {
    setTimeout(async () => {
        await driver.quit().catch((err) => console.log(err));
    }, 1000);
}

(async function example() {
    try {
        driver = await new Builder().forBrowser('firefox').build();
        await driver.get('http://minesweeperonline.com/');
        // await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
        // await driver.findElement(By.id('1_1')).click();
        runGame();
        // await driver.wait(until.titleIs('webdriver - Google search'), 1000);
    } finally {
        console.log('game end');
    }
})();