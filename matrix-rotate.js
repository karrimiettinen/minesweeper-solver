const display = require('./display');

let matrix = [
    [-1,-1,-1,-1],
    [ 0, 1, 2, 0],
    [ 0, 0, 0, 0]
    /*[-1,0,0],
    [-1,1,0],
    [-1,2,0],
    [-1,0,0]*/
];

console.log('original');
display.matrix(matrix,true);

matrix = matrix[0].map((val, index) => matrix.map(row => row[index]));
console.log('transposed');
display.matrix(matrix,true);

matrix = matrix[0].map((val, index) => matrix.map(row => row[index]));
console.log('attempt to turn it back to original');
display.matrix(matrix,true);
