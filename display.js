/**
 * This displays the matrix, remember to set boolean true for logging
 * @param {number[][]} matrix any 2 dimensional array
 * @param {boolean} log wether to display or not
 */
const matrix = (matrix, log) => {
    let displayString = "";
    for (let i = 0; i < matrix.length; i++) {
        displayString += "[";
        for (let j = 0; j < matrix[i].length; j++) {
            if (j === (matrix[i].length - 1)) {
                let digits = matrix[i][j];
                if (digits < 0 || digits > 9) {
                    displayString += digits + ",";
                } else {
                    displayString += " " + digits + ",";
                }
                displayString += "],\n";
            } else {
                let digits = matrix[i][j];
                if (digits < 0 || digits > 9) {
                    displayString += digits + ",";
                } else {
                    displayString += " " + digits + ",";
                }
            }
        }
    }
    if (log) {
        console.log(displayString);
    }
};

module.exports = {
    matrix
};